#!/bin/bash

TEST_DATA=test_data

function check_test_results {
for OUTPUT in $OUTPUTS; do
	diff "${TEST_DATA}/${TEST}_${OUTPUT}.out" "${TEST_DATA}/${TEST}_${OUTPUT}.expected"
	if [ $? -ne 0 ];then
		echo "${TEST} ${OUTPUT} FAILED!"
	else
		echo "${TEST} ${OUTPUT} PASSED"
	fi
done
}

# Test split file output
TEST=test_1
OUTPUTS="BC1-read-1 BC2-read-1 BC3-read-1 BC4-read-1 unmatched-read-1 multimatched-read-1 summary error"
rm -f "${TEST_DATA}\${TEST}*"
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter1.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "${TEST_DATA}/${TEST}_" --suffix .out --idxread 1 2> ${TEST_DATA}/${TEST}_error.out 1> ${TEST_DATA}/${TEST}_summary.out
if [ $? -ne 0 ];then
	echo "${TEST} ${OUTPUT} FAILED!"
else
	check_test_results
fi


# Test separate index file
TEST=test_2
OUTPUTS="BC1-read-1 BC2-read-1 BC3-read-1 BC4-read-1 unmatched-read-1 multimatched-read-1 BC1-read-2 BC2-read-2 BC3-read-2 BC4-read-2 unmatched-read-2 multimatched-read-2 summary error" 
rm -f ${TEST_DATA}\${TEST}*
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter1.txt" "${TEST_DATA}/barcode_splitter1.fastq" "${TEST_DATA}/barcode_splitter_index.fastq" --prefix "${TEST_DATA}/${TEST}_" --suffix .out --idxread 2 2> ${TEST_DATA}/${TEST}_error.out 1> ${TEST_DATA}/${TEST}_summary.out
if [ $? -ne 0 ];then
	echo "${TEST} ${OUTPUT} FAILED!"
else
	check_test_results
fi

