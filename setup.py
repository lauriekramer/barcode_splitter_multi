'''
Created on May 31, 2012

@author: rleach, lparsons
'''

from distutils.core import setup

#import sys
#try:
    # Load setuptools, to build a specific source package
#    from setuptools import setup
#except ImportError:
#    from distutils.core import setup


setup_args = dict(
    name="barcode_splitter_multi",
    version="0.14",
    #packages=['barcode_splitter_multi'],
    
    py_modules = ['barcode_splitter'],
      
    scripts = ['barcode_splitter.py'],

    data_files=[('', ['LICENSE.txt'])],
    
    # metadata for upload to PyPI
    author="Robert Leach, Lance Parsons",
    author_email="rleach@princeton.edu, lparsons@princeton.edu",
    description="A utility to split multiple sequence files using multiple sets of barcodes",
    license="BSD 2-clause",
    keywords="sequencing fastq paired barcode",
    url="https://bitbucket.org/hepcat72/barcode_splitter_multi",

    # could also include long_description, download_url, classifiers, etc.
)


#if 'setuptools' in sys.modules:
    #entry_points={
    #     'console_scripts': [
    #     'barcode_splitter = barcode_splitter_multi.barcode_splitter:main',
    #    ],
    #},
setup(**setup_args)
